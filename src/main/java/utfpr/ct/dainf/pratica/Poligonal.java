package utfpr.ct.dainf.pratica;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import java.util.ArrayList;
import java.util.List;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <T> Tipo do ponto
 */


/**Rever todos os vertices.**/

public class Poligonal<T extends Ponto2D> {
    private final List<T> vertices = new ArrayList<>();

    public void colocar(T p) {
        vertices.add(p);
    }
    
    public int getN(){
       return vertices.size();
    }
    
    public T get(int i){
        if(i<0 || i>vertices.size())//Conferir se isso dá certo
            throw new RuntimeException(String.format("get(%d): índice inválido", i));
        return vertices.get(i);
    }
    
    public void set(int i, T p){
        if(i==vertices.size()){
            vertices.add(p);
        }
        else if(i<0 || i>vertices.size())
            throw new RuntimeException(String.format("set(%d): índice inválido", i));
        else
            this.set(i, p);
    }
    
   public double getComprimento(){
        double retorno = 0.0;
        
        for(int i=0; i<getN()-1; i++)
            retorno+=get(i).dist(get(i+1));
                       
        return retorno;
   }
}

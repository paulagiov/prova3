/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 *
 * @author schirley
 */
public class PoligonalFechada extends Poligonal{
       @Override
               
   public double getComprimento(){
        double retorno = 0.0;
        int i;
        
        for(i=0; i<getN()-1; i++)
            retorno+=get(i).dist(get(i+1));
    
        retorno+=get(i).dist(get(0));
        
        return retorno;
   }
}
